<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="https://raw.githubusercontent.com/lombardpress/lombardpress-schema/develop/src/out/critical.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://raw.githubusercontent.com/lombardpress/lombardpress-schema/develop/src/out/critical.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xmlns:xi="http://www.w3.org/2001/XInclude">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>I, P. 1, Inq. 1, Tract. 2, Q. 4, Mem. III, C. 1</title>
        <author ref="#AlexanderOfHales">Alexander of Hales</author>
        <respStmt>
          <name xml:id="JW">Jeffrey C. Witt</name>
          <resp>TEI encoder</resp>
        </respStmt>
      </titleStmt>
      <editionStmt>
        <edition n="0.0.0-dev">
          <date when="2017-02-18">February 18, 2017</date>
        </edition>
      </editionStmt>
      <publicationStmt>
        <authority>
          <ref target="http://lydiaschumacher.wixsite.com/earlyfranciscans">ERC Project 714427: Authority and Innovation in Early Franciscan Thought</ref>
        </authority>
        <availability status="free">
          <p>Published under a 
            <ref target="http://creativecommons.org/licenses/by-nc-nd/3.0/">Creative Commons Attribution-NonCommercial-NoDerivs 3.0 License</ref>
          </p>
        </availability>
      </publicationStmt>
      <sourceDesc>
        <listWit>
          <witness xml:id="Q" n="quaracchi1924">Quaracchi 1924</witness>
        </listWit>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <schemaRef n="lbp-critical-1.0.0" url="https://raw.githubusercontent.com/lombardpress/lombardpress-schema/develop/src/out/critical.rng"/>
      <editorialDecl>
        <p>Encoding of this text has followed the recommendations of the LombardPress 1.0.0
          guidelines for a diplomatic edition.</p>
      </editorialDecl>
    </encodingDesc>
    <revisionDesc status="draft">
      <listChange>
        <change when="2017-02-18" status="draft" n="0.0.0">
          <p>Created file for the first time.</p>
        </change>
      </listChange>
    </revisionDesc>
  </teiHeader>
  <text xml:lang="la">
    <front>
      <div xml:id="include-list">
        <xi:include href="https://raw.githubusercontent.com/lombardpress/lombardpress-lists/master/workscited.xml" xpointer="worksCited"/>
        <xi:include href="https://raw.githubusercontent.com/lombardpress/lombardpress-lists/master/Prosopography.xml" xpointer="prosopography"/>
      </div>
      <div xml:id="starts-on">
        <pb ed="#Q" n="99"/><cb ed="#Q" n="a"/>
      </div>
    </front>
    <body>
      <div xml:id="ahsh-l1p1i1t2q4m3c1">
        <head xml:id="ahsh-l1p1i1t2q4m3c1-Hd1e3729">I, P. 1, Inq. 1, Tract. 2, Q. 4, Mem. III, C. 1</head>
        <head xml:id="ahsh-l1p1i1t2q4m3c1-Hd1e3732" type="question-title">DE ESSENTIA AEVI</head>\
        <p xml:id="ahsh-l1p1i1t2q4m3c1-d1e3767">
          <lb ed="#Q"/>Quantum ad primum obicitur sic: 1. Omnis du<lb ed="#Q"/>ratio,
          sive sit esse creati sive increati, reducitur 
          <lb ed="#Q"/>ad aliquod 'nunc ' vel praesens. Si ergo non est
          <lb ed="#Q"/>ponere nisi 'nunc' durationis temporalis et dura<lb ed="#Q"/>tionis
          aeternae, ergo non erit nisi duplex dura<lb ed="#Q"/>tio:
          aeterna et temporalis; ergo non est nisi aut
          <lb ed="#Q"/>duratio quae est tempus, aut duratio quae est ae<lb ed="#Q"/>ternitas
          ; aevum igitur non est aliqua duratio sepa<lb ed="#Q"/>rata
          a tempore et aeternitate. -— Quod autem
          <lb ed="#Q"/>nunc' non sit nisi duplex, patet per Boe<lb ed="#Q"/>thium,
          in libro De Trinitate 3, ubi dicit: « Nostrum
          <lb ed="#Q"/>nunc', quasi '" currens tempus, tacit sempiternita<lb ed="#Q"/>tem;
          divinum 'nunc' permanens aeternitatem ».
        </p>
        <p xml:id="ahsh-l1p1i1t2q4m3c1-d1e3798">
          <lb ed="#Q"/>2. Item, Augustinus, Xl Canfessianum4: 
          <lb ed="#Q"/>' Praesens, si semper est praesens, iam non est
          <lb ed="#Q"/>tempus, sed aeternitas »; sed praesens in esse per<lb ed="#Q"/>petuorum
          semper est praesens; ergo est aeter<cb ed="#Q" n="b"/><lb ed="#Q"/>nitas; 
          si ergo ponitur mensura esse perpetui, in
          <lb ed="#Q"/>quantum huiusmodi, aevum non erit aliud quam
          <lb ed="#Q"/>aeternitas.
        </p>
        <p xml:id="ahsh-l1p1i1t2q4m3c1-d1e3817">
          <lb ed="#Q"/>3. Item, quasi * ex opposito obicitur: diversitas
          <lb ed="#Q"/>materiae non diversificat rationem formae, ma<lb ed="#Q"/>xime
          in formis accidentalibus; unde dicit Philo<lb ed="#Q"/>sophus5
          quod eodem denario mensurantur'
          <lb ed="#Q"/>decem homines et decem equi, non tamen sunt
          <lb ed="#Q"/>eadem decem. Similiter eadem mensura duratio<lb ed="#Q"/>nis
          mensurabitur temporale et aeternum; licet non
          <lb ed="#Q"/>sit idem temporale et aeternum, tamen una erit
          <lb ed="#Q"/>mensura, quae proportionabitur cuilibet mensu<lb ed="#Q"/>ratorum
          secundum quod ei conveniet. Erit igitur
          <lb ed="#Q"/>quod nos dicimus 'aevum'm non separata du<lb ed="#Q"/>ratio
          secundum speciem et tempore.
        </p>
        <p xml:id="ahsh-l1p1i1t2q4m3c1-d1e3846">
          <lb ed="#Q"/>4. Item, si aevum est separata duratio ab ae<lb ed="#Q"/>ternitate 
          et tempore, quaeritur utrum sit substan<pb ed="#Q" n="100"/><cb ed="#Q" n="a"/><lb ed="#Q"/>tia 
          vel accidens. Non potest poni substantia:
          <lb ed="#Q"/>quia, cum aevum ponatur duratio creaturae per<lb ed="#Q"/>petuae,
          si esset substantia, non esset nisi sub<lb ed="#Q"/>stantia
          cuius est mensura, et ita esset in perpe<lb ed="#Q"/>tuis
          substantiis creatis idem a essentia et sua du<lb ed="#Q"/>ratio;
          quod nullo mddo est, nisi in sola increata
          <lb ed="#Q"/>essentia: ipsa enim increata substantia est sua
          <lb ed="#Q"/>duratio et sua aeternitas; quod nullo modo con<lb ed="#Q"/>tingit
          in aliis. — Item, si ponatur accidens, non
          <lb ed="#Q"/>ponetur nisi quantitas cuius proprium est mensu<lb ed="#Q"/>rare:
          aut ergo ponetur continua aut discreta. Non
          <lb ed="#Q"/>ponetur discreta: quia sic" esset in divisione, ut
          <lb ed="#Q"/>numerus et oratio. Si vero ponatur continua: aut
          <lb ed="#Q"/>ergo ponetur permanens aut successiva. Si per<lb ed="#Q"/>manens:
          ergo esset autc linea aut superficies aut
          <lb ed="#Q"/>corpus; quod nullo modo potest poni. Ponetur
          <lb ed="#Q"/>ergo quantitas continua successiva: ergo erit tem<lb ed="#Q"/>pus.
          Quocumque ergo modo dicatur aevum ",
          <lb ed="#Q"/>non ponetur separata duratio ab aeternitate et
          <lb ed="#Q"/>tempore.
        </p>
        <p xml:id="ahsh-l1p1i1t2q4m3c1-d1e3898">
          <lb ed="#Q"/>5. Item, Eccli. 43,6: Luna ostensio temporis
          <lb ed="#Q"/>et signum aevi. Et exponit Rabanusl quod
          <lb ed="#Q"/>ludaei intelligebant tempora penes cursum lunae.
          <lb ed="#Q"/>Secundum hoc ergo luna est ostensio temporis, id
          <lb ed="#Q"/>est differentiarum et partium temporis, et etiam
          <lb ed="#Q"/>signum aevi, id est' durationis totius temporis,
          <lb ed="#Q"/>quae per aevum poterit mensurari.
        </p>
        <p xml:id="ahsh-l1p1i1t2q4m3c1-d1e3916">
          <lb ed="#Q"/>Contra: a. Eccli. 1,1: Omnis sapientia a Do<lb ed="#Q"/>mino
          Deo est et cum illa fuit semper et est ante
          <lb ed="#Q"/>aevum. Igitur cuius duratio est aeternitas, prae<lb ed="#Q"/>cedit
          aevum; ergo est duratio sequens aeternita<lb ed="#Q"/>tatem;
          ergo alia ab aeternitate.
        </p>
        <p xml:id="ahsh-l1p1i1t2q4m3c1-d1e3930">
          <lb ed="#Q"/>b. Item, Boethiusz: «Tempus ab aevo ire
          <lb ed="#Q"/>iubes »; sed quod exit ab aliquo non est idem
          <lb ed="#Q"/>illi; ergo aeVum est duratio differens a tempore.
        </p>
        <p xml:id="ahsh-l1p1i1t2q4m3c1-d1e3939">
          <lb ed="#Q"/>c. Item, Dionysius, in libro De divinis no—
          <lb ed="#Q"/>minibus3: « Proprietas aevi est antiquum, inalte<lb ed="#Q"/>ratum,
          secundum totum esse mensurans; tempus
          <lb ed="#Q"/>vero est quod est in generatione et corruptione
          <lb ed="#Q"/>aliter se habens ».
        </p>
        <p xml:id="ahsh-l1p1i1t2q4m3c1-d1e3952">
          <lb ed="#Q"/>d. Item, Augustinus, inf 83 Quaestionf:
          « In hoc distat tempus ab aevo, quia istud sta<lb ed="#Q"/>bile,
          illud autem mutabile ». Ergo non est idem
          <lb ed="#Q"/>tempori 2.
        </p>
        <p xml:id="ahsh-l1p1i1t2q4m3c1-d1e3961">
          <lb ed="#Q"/>e. Item, est esse sine principio et sine fine,
          <lb ed="#Q"/>non incipiens a non esse nec vertibile in non
          <lb ed="#Q"/>esse, ut aeternum; et est esse cum principio et
          <lb ed="#Q"/>fine, incipiens a non esse et vertibile in non esse ",
          <lb ed="#Q"/>ut temporale; et est esse habens principium et
          <lb ed="#Q"/>incipiens a non esse, sed non habens finem nec
          <cb ed="#Q" n="b"/>
          <lb ed="#Q"/>est vertibile in non esse, ut perpetuum. Si ergo
          <lb ed="#Q"/>durationes differunt secundum differentem exten<lb ed="#Q"/>sionem
          esse, cum triplex sit extensio esse, ut dic<lb ed="#Q"/>tum
          est 5, seu quasi extensio sive protensio, erunt
          <lb ed="#Q"/>tres durationes; ergo cum duratio primi sit aeter<lb ed="#Q"/>nitas,
          duratio secundi' tempus, duratio tertii erit
          <lb ed="#Q"/>aevum.
        </p>
        <p xml:id="ahsh-l1p1i1t2q4m3c1-d1e3994">
          <lb ed="#Q"/>Solutio: Quod concedendum est. Notandum
          <lb ed="#Q"/>autem quodk nomen aevi dicitur multipliciter.
          <lb ed="#Q"/>Aliquando enim aevum accipitur pro omni dura<lb ed="#Q"/>tione
          quae non est tempus, secundum quod dicit
          <lb ed="#Q"/>Dionysius 6: « Proprietas aevi antiquum » etc.
          <lb ed="#Q"/>Aliquando vero pro tota duratione temporis, secun<lb ed="#Q"/>dum
          quod dicit Dionysius, in libro De divinis
          <lb ed="#Q"/>nominibus: «Aliquando totam durationem tem<lb ed="#Q"/>poris,
          quae' apud nos est, aevum appellat»; et
          <lb ed="#Q"/>hoc modo accipitur Eccli. 43,6: Luna ostensio
          <lb ed="#Q"/>temporis et signum aevi. Aliquando vero pro an—
          <lb ed="#Q"/>tiquitate temporis; unde DiOnysi us: « Saepe an<lb ed="#Q"/>tiquissima
          aevi cognominatione'" fingunt ». Ali—
          <lb ed="#Q"/>quando pro magna parte temporis: ab Adam usque
          <lb ed="#Q"/>ad Noe, unum aevum. Aliquando vero pro periodo
          <lb ed="#Q"/>sive duratione rei alicuius, secundum quod tota
          <lb ed="#Q"/>vita alicuius hominis appellatur suum aevum.
          <lb ed="#Q"/>Aliquando vero accipitur secundum proprium mo—
          <lb ed="#Q"/>dum, secundum quod aevum est duratio rei ha<lb ed="#Q"/>bentis
          esse post non esse, sed non vertibilis in
          <lb ed="#Q"/>non esse, ut in perpetuis.
        </p>
        <p xml:id="ahsh-l1p1i1t2q4m3c1-d1e4042">
          <lb ed="#Q"/>[Ad obiecta]: ]. Ad primum ergo dicendum
          <lb ed="#Q"/>quod Boethius non negavit'nunc' aevi, sed
          <lb ed="#Q"/>stabilitatem 'nunc' sive praesentis aeternitatis
          <lb ed="#Q"/>declaravit per comparationem ad 'nunc' tem<lb ed="#Q"/>poris,
          quod nobis magis manifestum est. Triplex
          <lb ed="#Q"/>tamen est 'nunc'. Est enim 'nunc', quod non
          <lb ed="#Q"/>incepit esse et quod immutabiliter manet in esse:
          <lb ed="#Q"/>et est 'nunc' aeternitatis; et 'nunc', quod in<lb ed="#Q"/>cipit
          esse, sed non mutatur nec fluit in'[ esse: et
          <lb ed="#Q"/>tale est 'nunc' aevi, et dicitur hic fluxus suc<lb ed="#Q"/>cessionis
          variabilis et non continuationis esse
          <lb ed="#Q"/>sive durationis; et est 'nunc ' quod incipit esse
          <lb ed="#Q"/>et mutatur et fluit in esse: et tale est 'nunc'
          <lb ed="#Q"/>temporis. Praesens enim aeternitatis semper fuit
          <lb ed="#Q"/>et est et erit; uno modo ens praesens perpetui
          <lb ed="#Q"/>non semper fuit, sed semper et est et erit; uno
          <lb ed="#Q"/>modo ens praesens temporis mutatur et fluit a
          <lb ed="#Q"/>futuro in praesens, a praesenti in praeteritum.
        </p>
        <p xml:id="ahsh-l1p1i1t2q4m3c1-d1e4085">
          <lb ed="#Q"/>2. Ad secundum dicendum quod 'semper'
          <lb ed="#Q"/>uno modo accipitur pro toto tempore, alio modo
          <lb ed="#Q"/>pro tota aeternitate, quae non habet totalitatem
          <lb ed="#Q"/>partibilitatis, sed perfectionis. Intelligitur ergo
          <pb ed="#Q" n="101"/>
          <cb ed="#Q" n="a"/>
          <lb ed="#Q"/>'semper ', cum dicitur « praesens, si semper est
          <lb ed="#Q"/>praesens » etc., de totalitate aeternitatis in dura<lb ed="#Q"/>tione
          quae non incipit nec desinit, si accipiatur
          <lb ed="#Q"/>aeternitas proprie, et non de totalitate temporis:
          <lb ed="#Q"/>praesens enim perpetui praesens est toti tem<lb ed="#Q"/>pori.
        </p>
        <p xml:id="ahsh-l1p1i1t2q4m3c1-d1e4114">
          <lb ed="#Q"/>3. Ad tertium dicendum quod quantitas est
          <lb ed="#Q"/>duobus modis, sicut dicit B." Augustinus, in
          <lb ed="#Q"/>libro De quantitate animae]: est enim quantum
          <lb ed="#Q"/>virtute, quantum dimensione. Dicendum ergo quod
          <lb ed="#Q"/>aevum accidens est rei aeviternae, id est perpe<lb ed="#Q"/>tuae,
          et reducetur ad genus quantitatis secundum
          <lb ed="#Q"/>quod extenditur quantitas ad extensionem virtua<cb ed="#Q" n="b"/><lb ed="#Q"/>lem; 
          secundum vero quod coarctatur intentio quan<lb ed="#Q"/>titatis
          ad quantitatem dimensionis, procedebat
          <lb ed="#Q"/>obiectio.
        </p>
        <p xml:id="ahsh-l1p1i1t2q4m3c1-d1e4139">
          <lb ed="#Q"/>4. Ad quartum dicendum quod est accidens
          <lb ed="#Q"/>proprium et est" accidens commune. Accidens
          <lb ed="#Q"/>proprium, ut risibile homini, quode per se et
          <lb ed="#Q"/>omni inest et soli, diversificatur secundum diffe<lb ed="#Q"/>rentiam
          specieid subiecti. Accidens vero com<lb ed="#Q"/>mune,
          ut albedo et numerus homini 9, non diver<lb ed="#Q"/>sificatur
          secundum speciem, sed solum secundum
          <lb ed="#Q"/>numerum. Exf accidentibus autem propriis est
          <lb ed="#Q"/>aevum aevitemis et tempus temporalibus; et ideo
          <lb ed="#Q"/>differunt specie tempus et aevum 2.
        </p>
      </div>
    </body>
  </text>
</TEI>